# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import cv2
import configparser as cp

class cameraConnector:
    
    def getConnectionString(self,config):
        
        """
        Input : Database Connection Parameters from config file
        Output : Connection String to connect with Database
        
        """
        
        # Database Connection parameters
        dialect = config.get('Database_Connection', 'dialect')
        host = config.get('Database_Connection', 'host')
        port = config.get('Database_Connection', 'port')
        username = config.get('Database_Connection', 'username')
        password = config.get('Database_Connection', 'password')
        dbname = config.get('Database_Connection', 'dbname')
        
        conn_string = dialect+'://'+username+':'+password+'@'+host+':'+port+'/'+dbname
        return conn_string
    
    def getCameraDetails(self, config, conn_string,camera_serial):
        
        """
        Input : Database Connection String, Camera Name/Serial number
                    (The Camera meta data will be fetched from RDBMS
                    after creating a connection with the Database)
        Output : Port Number and Host Name
        
        """
        # Camera Connection parameters
        host = config.get('Camera_Metadata', 'host')
        port = config.get('Camera_Metadata', 'port')
        
        cam_meta = host+':'+port
        return cam_meta
    
    def getAuthCredentials(self, config, conn_string,camera_serial):
        
        """
        Input : Database Connection String, Camera Name/Serial number
                    (The Authentication Credentials data will be fetched from RDBMS
                    after creating a connection with the Database)
        Output : Protocol type, Username and Password 
        
        """    
    
        # Authentication Credentials parameters
        protocol = config.get('Auth_Credentials', 'protocol')
        username = config.get('Auth_Credentials', 'username')
        password = config.get('Auth_Credentials', 'password')
        
        auth_cred = protocol+"://"+username+":"+password
        return auth_cred
    
    def getConnectionMeta(self, config, conn_string,camera_serial):
        
        """
        Input : Database Connection String, Camera Name/Serial number
                    (The Connection meta data will be fetched from RDBMS
                    after creating a connection with the Database)
        Output : Connection Meta String
        
        """ 
        
        #  Connection Meta Data parameters
        path =  config.get('Connection_Meta', 'path')
        channel = config.get('Connection_Meta', 'channel')
        subtype = config.get('Connection_Meta', 'subtype')
        unicast = config.get('Connection_Meta', 'unicast')    
        proto = config.get('Connection_Meta','protocol')
        connection_meta = path+"?channel="+channel+"&subtype="+subtype+"&unicast="+unicast+"&proto="+proto
        return connection_meta
    
    def createStreamingURL(self, cam_meta,auth_meta,connection_meta):
            
        """
        Input : Camera Meta String, Auth Credential String, Connection Metadata String
        Output : Streaming URL
        
        """ 
        streaming_url = auth_meta+"@"+cam_meta+"/"+connection_meta
        return streaming_url
    
    def fetchVideoFrames(self, streaming_url): 
        print(streaming_url)
        
        """
        Input : Streaming URL
        Output : Frames
        
        """ 
        cap= cv2.VideoCapture(streaming_url)
        
        return cap
            
        
    def directConnector(self):
        
        config = cp.ConfigParser()
        config.read('config.ini')
        
        conn_string = self.getConnectionString(config)
        camera_serial = "CAMERA3"
        
        cam_meta = self.getCameraDetails(config, conn_string,camera_serial)
        
        auth_cred = self.getAuthCredentials(config, conn_string,camera_serial)
        
        connection_meta = self.getConnectionMeta(config, conn_string,camera_serial)
        
        streaming_url = self.createStreamingURL(cam_meta,auth_cred,connection_meta)
        
        cap = self.fetchVideoFrames(streaming_url)
        
        fps = cap.get(cv2.CAP_PROP_FPS)
        
        ret = (cap,fps)
            
        """
        In reality, further functions would be written and a frame would be fed to 
        these functions to fetch out results
        """
        return ret
        

if __name__ == "__main__":
    msg = cameraConnector()
    print(msg.directConnector())

    
    
    
    